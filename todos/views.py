from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm, TodoItemForm


def todo_list_list(request):
    a_todo_list = TodoList.objects.all()
    context = {
        "todo_list_key": a_todo_list,
    }
    return render(request, "todo_lists/todo.html", context)


def todo_list_detail(request, id):
    important_todo_list = TodoList.objects.get(id=id)
    context = {
        "todo_object": important_todo_list,
    }
    return render(request, "todo_lists/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()

            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todo_lists/create.html", context)


def todo_list_update(request, id):
    post = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save()
            return redirect("todo_list_detail", id=post.id)
    else:
        form = TodoListForm(instance=post)

        context = {
            "form": form,
        }
    return render(request, "todo_lists/edit.html", context)


def todo_list_delete(request, id):
    if request.method == "POST":
        list = TodoList.objects.get(id=id)
        list.delete()
        return redirect("todo_list_list")

    return render(request, "todo_lists/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()

            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todo_lists/create_items.html", context)
